package com.seartipy.algos

 object ArrayUtils {
   def minIndex(arr: Array[Int], from: Int = 0, to:Int = -1) = {
     val end = if(to == -1)  arr.length else to
     var mi = from
     for(i <- from + 1 until end)
       if(arr(mi) > arr(i))
         mi = i
     mi
   }

  def swap(arr:Array[Int], x: Int, y: Int) {
    val temp = arr(x)
    arr(x) = arr(y)
    arr(y) = temp
  }
}
