package com.seartipy.algos

object BubbleSort {

  def apply(arr: Array[Int]) {
    var i = 0
    var swapped = false
    do {
      swapped = false
      for(j <- 0 until arr.size - i - 1 if arr(j) > arr(j+1)) {
          ArrayUtils.swap(arr, j, j + 1)
          swapped = true
      }
      i += 1
    }while(swapped)
  }
}
