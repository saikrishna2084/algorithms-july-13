package com.seartipy.algos;

object BinarySearch {
  private def binarySearch(arr: Array[Int], value: Int, low: Int, high: Int) : Int = {
    var lo = low
    var hi = high
    while(lo <= hi) {
      val mid = (lo + hi) / 2
      if(value == arr(mid))
        return mid
      if(value < arr(mid))
        hi = mid - 1
      else
        lo = mid + 1
    }
    -1
  }
  def apply(arr: Array[Int], value: Int) = {
    binarySearch(arr, value, 0, arr.length - 1)
  }
}

