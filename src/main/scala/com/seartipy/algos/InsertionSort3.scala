package com.seartipy.algos

object InsertionSort3 {
  def apply(arr:Array[Int]) {
    for(i <- 1 until arr.length)
      insertSorted(arr, i)
  }

  // Insert arr[i] in arr[0..i]
  private def insertSorted(arr: Array[Int], i: Int) {
    var j = i
    val v = arr(i)
    while (j > 0 && v < arr(j - 1)) {
      arr(j) = arr(j - 1)
      j -= 1
    }
    arr(j) = v
  }
}
