package com.seartipy.algos

import com.seartipy.algos.ArrayUtils._

object SelectionSort {

  def apply(arr: Array[Int]) {
    for(i <- 0 until arr.length)
    swap(arr, i, minIndex(arr, i))
  }

}
