package com.seartipy.algos

object InsertionSort2 {
  def shiftRight(arr:Array[Int], start: Int, stop: Int) {
    for(i <- stop until start by -1)
      arr(i) = arr(i - 1)
  }

  /* Insert arr[end] in 'arr[0...end]' */
  def insertSorted(arr:Array[Int], end: Int) {
    val value = arr(end)
    for(i <- 0 until end)
      if(value < arr(i)) {
        shiftRight(arr, i, end)
        arr(i) = value
        return
      }
  }

  def apply(arr:Array[Int]) {
     for(i <- 1 until arr.length)
       insertSorted(arr, i)
  }
}
