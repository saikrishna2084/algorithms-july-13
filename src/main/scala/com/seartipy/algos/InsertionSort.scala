package com.seartipy.algos

object InsertionSort {
  /** arr[0...pos] is sorted. Insert arr[pos] in arr[0...pos] so that the arr[0...pos+1] is sorted. **/
  def insertSorted(arr: Array[Int], pos: Int) {
    for(i <- pos until 0 by -1) {
      if(arr(i - 1) <= arr(i))
        return
      ArrayUtils.swap(arr, i, i - 1)
    }
  }

  def apply(arr: Array[Int]) {
     for(i <- 1 until arr.length)
       insertSorted(arr, i)
  }

}
