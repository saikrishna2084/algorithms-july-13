package com.seartipy.algos

import ArrayUtils._
import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers

class ArrayUtilsTest extends FunSpec with ShouldMatchers {

  describe("minIndex") {

    it("single element array") {
      val arr = Array(100)
      minIndex(arr) should be (0)
    }

    it("two element array") {
      var arr = Array(1, 2)
      minIndex(arr) should be (0)
      arr = Array(2, 1)
      minIndex(arr) should be (1)
    }

    it("multiple element array") {
      val arr = Array(11, 2, 3, 5, 7)
      minIndex(arr) should be (1)
    }

    it("multiple element array with smallest at first") {
      val arr = Array(1, 5,7,2,8)
      minIndex(arr) should be (0)
    }

    it("multiple element array with smallest at last") {
      val arr = Array(11, 5,7,2,8, 0)
      minIndex(arr) should be (5)
    }
  }

  describe("swap") {

    it("swaps two elements of an array") {
      val arr = Array(1, 2)
      swap(arr, 0, 1)
      arr(0) should be (2)
      arr(1) should be (1)
    }
  }
}
