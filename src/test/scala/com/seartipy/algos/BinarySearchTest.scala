package com.seartipy.algos


import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers

class BinarySearchTest extends FunSpec with ShouldMatchers {

  describe("BinarySearch") {

    it("empty array") {
      val arr: Array[Int] = Array()
      BinarySearch(arr, 1) should be (-1)
    }

    it("single element array") {
      val arr: Array[Int] = Array(100)
      BinarySearch(arr, 1) should be (-1)
      BinarySearch(arr, 100) should be (0)
    }

    it("array with length odd") {
      val arr: Array[Int] = Array(1,2,3,4,5)
      BinarySearch(arr, 1) should be (0)
      BinarySearch(arr, 5) should be (4)
      BinarySearch(arr, 2) should be (1)
      BinarySearch(arr, 4) should be (3)
      BinarySearch(arr, 3) should be (2)
      BinarySearch(arr, 0) should be (-1)
      BinarySearch(arr, 6) should be (-1)
    }

    it("array with length even") {
      val arr: Array[Int] = Array(1,2,3,4,5,6)
      BinarySearch(arr, 1) should be (0)
      BinarySearch(arr, 5) should be (4)
      BinarySearch(arr, 2) should be (1)
      BinarySearch(arr, 4) should be (3)
      BinarySearch(arr, 3) should be (2)
      BinarySearch(arr, 6) should be (5)
      BinarySearch(arr, 0) should be (-1)
      BinarySearch(arr, 7) should be (-1)
    }
  }
}
