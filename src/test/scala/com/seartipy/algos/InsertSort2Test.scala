package com.seartipy.algos


import org.scalatest.FunSpec
import org.scalatest.matchers.ShouldMatchers

class InsertSort2Test extends FunSpec with ShouldMatchers{

  describe("InsertionSort2") {

    it("sorts an empty array") {
      val arr: Array[Int] = Array()
      InsertionSort2(arr)
      arr should equal (Array())
    }

    it("sorts a single element array") {
      val arr = Array(100)
      InsertionSort2(arr)
      arr should equal (Array(100))
    }

    it("sorts array of even length") {
      val arr = Array(5, 4, 9, 2, 1, 8, 0, 3, 7, 6)
      InsertionSort2(arr)
      arr should equal (Array(0,1,2,3,4,5,6,7,8,9))
    }

    it("sorts array of odd length") {
      val arr = Array(5, 4, 9, 2, 1, 8, 3, 7, 6)
      InsertionSort2(arr)
      arr should equal (Array(1,2,3,4,5,6,7,8,9))
    }

    it("sorts array with largest element first") {
      val arr = Array(9, 4, 5, 2, 1, 8, 0, 3, 7, 6)
      InsertionSort2(arr)
      arr should equal (Array(0,1,2,3,4,5,6,7,8,9))
    }

    it("sorts array with smallest element last") {
      val arr = Array(5, 4, 9, 2, 1, 8, 6, 3, 7, 0)
      InsertionSort2(arr)
      arr should equal (Array(0,1,2,3,4,5,6,7,8,9))
    }
  }
}
