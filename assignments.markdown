#Assignments

##Assignment 5(25th July 2013)
###C#
1. Implement all algorithms using IEnumerable<T> and IList<T>
2. Read about IEquatable and implement all algorithms using IEquatable<T> and IComparable<T>
3. Implement algorithms using Function Objects(Delegates) where possible

###Javascript
1. Implement all algorithms using functions as parameters for comparing
2. Try implementing a generic deepEquals method
3. Implement array_compare function to compare arrays taking compare function as parameter to compare individual elements.

##Assignment 4(24th July 2013)
1. Implement linear_search and binary_search.
2. Can effeciency of linear_search be improved? Read about sentinels and provide an implementation.

### C#
1. Read about generics, constraints,covariance and contravariance.
2. Implement all code using generics. Use IComparable<T> instead of IComparable

###C++
1. Read about locality of reference. Understand it in relation with binary_search.
2. Read about concepts. Read Copyable, Assignable, Equitable etc and implement search algorithms using Equitable concept.

###Javascript

1. How do you deal with value equality in javascript for user defined objects? Research and provide your solution.
2. using sentinels is easy in javascript as arrays can grow. Please provide an implementation.

##Assignment 3(23nd July 2013)

### C#, Javascript and C++

1. Implement effecient Insertion Sort
2. Improve effeciency of bubble_sort in my code(or your code if it's not effecient).
3. Implement generic linear search.

###C++
1. Implement effecient swap using move semantics
2. Measure performance difference between selection_sort, bubble_sort and insertion_sort.

###C#

1. Learn interfaces. Learn explicit implementation of interface methods.

###Javascript

1. Implement all sorts taking a compare function for comparing elements.

##Assignment 2 (19th July 2013) 

### C#, Javascript and C++

1. Implement selection_sort and insertion_sort
2. Implement string_match(string, sub_string method) to check if sub_string is contained in string. 
3. Check if the bubble_sort is effecient enough. If not fix it.
4. Think if abstraction of sort algorithm which can have different implementations like quick_sort, insertion_sort etc will be useful. If yes, give an example. If no, provide a convincing argument.

##Assignment 1 (18th July 2013) 

### C# 

1. Implement IsSorted, BubbleSort. Write Unit tests using xunit.
 
####Optional 
 
2. Read about IsComparable. Make both algorithms more generic using IComparable.
3. Read about generics in C# and try to implement generic implementations of both algorithms.


###Javascript

1. learn about creating modules using comonjs(node js modules)
2. Implement is_sorted and bubble_sort part of algorithms.sorting module.
3. Implement unit tests using mochajs

###C++

1. learn CATCH unit testing framework, read a bit about scons. 
2. learn templates, iterators and LessThanComparable concept.
3. Implement is_sorted, selection_sort using iterators and LessThanComparable concept.
